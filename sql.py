from flask import Flask
from flask_mysqldb import MySQL

import pymysql


app = Flask(__name__)
mysql = MySQL(app=None)
# app.config.update(dict( DATABASE="EMS_DB", USERNAME=" ", HOST="mysql://localhost:3306/EMS_DB?zeroDateTimeBehavior=convertToNull", PASSWORD=" " ))
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'EMS_DB'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

mysql.init_app(app)

@app.route('/')
def index():
    cursor =  mysql.connection.cursor()
    cursor.execute('''SELECT * FROM admins ''')
    rv = cursor.fetchall()
    return str(rv)

if __name__ == '__main__':
    app.run(debug=True)
