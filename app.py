import os
from flask import Flask, jsonify, request, render_template, url_for,redirect
from flask_restful import Resource, Api

#Video processing library
from moviepy.editor import *
from moviepy.video.fx import resize, mirror_x

# Password Encryption
from werkzeug.utils import secure_filename


# TESTING VIDEO PROCESSING WITH STATIC MEDIA FILES


# Media Files Allowed Extensions
UPLOAD_FOLDER = 'static/vids'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# Flask Init
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
api = Api(app)


# Login Main Routing 
@app.route('/')
def login():
    return render_template("login.html")
    
#Dashboard Routing
@app.route('/ems', methods=['POST'])
def dashboard():
    return render_template("dashboard.html")

#Define allowed extensions
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

#Media File Upload
@app.route('/save_video', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return file.filename,200


# w = 720
# h = w*9/16 # 16/9 screen
# moviesize = w,h
# overlay title on clip
video = VideoFileClip("static/vids/clip3.mp4")

# Make the text. Many more options are available.
title = (TextClip("¡Lost Kid!", fontsize=30,
                  font="Arial",
                  stroke_color=('black'))
         .margin(top=10, opacity=0)
         .set_position(("center", "top")))


img_main = ImageClip(
    "static/images/lost-kid.jpeg").resize(0.2).margin(left=110, top=60, opacity=0)

img_left = ImageClip(
    "static/images/img-alert.png").resize(0.1).margin(top=85, opacity=0)

img_right = ImageClip(
    "static/images/img-alert.png").resize(0.1).margin(left=260, top=85, opacity=0)


# *********** Text Styling Variables ***********
text_top = 170

# *********** ********************** ***********

text_1 = (TextClip('Name: Mike',
                   fontsize=25,
                   font="Century-Schoolbook-Roma",
                   color='yellow', method='label')
          .set_position('center')
          .margin(top=text_top, opacity=0)
          .set_duration(3)
          .set_start(0))

text_2 = (TextClip('Age: 14',
                   fontsize=25,
                   font="Century-Schoolbook-Roman",
                   color='yellow')
          .set_position('center')
          .margin(top=text_top, opacity=0)
          .set_duration(3)
          .set_start(3))

text_3 = (TextClip('Lost since last tuesday 14th',
                   fontsize=20,
                   font="Century-Schoolbook-Roman",
                   color='yellow')
          .set_position('center')
          .margin(top=text_top, opacity=0)
          .set_duration(3)
          .set_start(6))

text_4 = (TextClip('Call: 01-800-999-999',
                   fontsize=25,
                   font="Century-Schoolbook-Roman",
                   color='yellow')
          .set_position('center')
          .margin(top=text_top, opacity=0)
          .set_duration(9, 12)
          .set_start(9))

# ************ Final Video Composition ************

final_clip = CompositeVideoClip(
    [
        video,
        title,
        img_main,
        text_1, text_2, text_3, text_4,
        img_left,
        img_right
    ]
).set_duration(video.duration)  # Overlay text on video
# final_clip.write_videofile("edit.avi", codec='avi', fps=35)
#

#


# result.ipython_display(t=500,width=1400)

app.run(port=5001)
if __name__ == "__main__":
    app.run(debug=True)
