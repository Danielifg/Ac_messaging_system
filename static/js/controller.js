
$("document").ready(function() {

// Loading points animation when deploying message
var dots = 0;
function type() {
  if (dots < 3) {
    $('#dots').append('.');
    dots++;
  } else {
    $('#dots').html('');
    dots = 0;
  }
}
setInterval(type, 600);

  // Create new message , Show editing panel
  $('#btn-new-msg').click(function() {
    $('#input_new_msg_title').show();
    $('#blur-screen').show();
    $('#video-edit-form').show();
  });


  // Js clock dashboard right hand corner
  function time() {
    var d = new Date();
    document.getElementById("tx").innerHTML = d.toLocaleTimeString();
  }
  setInterval(time, 1000);

 
  // Array to hold preset messages
  var $messages = [];


  // Ajax call init to query Messages Table 
  
  $.ajax({
    headers: {
      'Content-Type': 'application/json'
    },
    crossDomain: true,
    dataType: "json",
    type: "GET",
    url: "webresources/messagesList",
            beforeSend: function () {
                $('.hexa-loader').show();       // Loader img while request
                $('.loading-cover').show();
            },
    success: function(data, textStatus, jqXHR) {
      // Load Messasges from DB
      $.each(data, function(index) {
        $messages.push(data[index].messageTitle);
        var $i = index + 1; // avoid index 0
        var $id = data[index].messageTitle.split(' ').join('_'); //join value to apply as element id
        $("#messages-table").find('tbody') //add table rows as per data from DB
          .append($('<tr id=' + $id +
            '><th>' + $i + '</th><td>' +
            data[index].messageTitle + '</td>' +
            '<td><img src=' +
            'resources/images/ui-icons/icon-config-queue2.png' +
            ' style=' + 'width:20px;float:right;z-index:20;' +
            ' class=' + 'icon-config' + ' /></td>' +
            '<td><img src=' +
            'resources/images/ui-icons/icon-play-video.png' +
            ' alt=' + 'Play Video' +
            ' style=' +
            'width:20px;margin-left:50%;z-index:20;' +
            ' id=' + 'icon-play-video' + ' />' +
            '</td>'));
      });
    },
    error: function(data, textStatus, jqXHR) {
      console.log("ajax call failed");
      console.log("textStatus = " + textStatus);
      console.log("jqXHR s= " + jqXHR);
      console.log("jqXHR.status = " + jqXHR.status);
    }
  });
  
        //  Messages List Selection 
      $(".icon-config").on('click', function(event) {
        $('#blur-screen').show();
        $('#video-edit-form').show();

      });
      // Show editing panel
            $('#btn-new-msg').click(function() {
        $('#blur-screen').show();
        $('#video-edit-form').show();
      });

  // 1st Ajax call end 



  // Ajax Retrives QUEUE Selection from DB
  $.ajax({
    headers: {
      'Content-Type': 'application/json'
    },
    crossDomain: true,
    dataType: "json",
    type: "GET",
    url: "webresources/messagesQueue",
            beforeSend: function () {
                $('.hexa-loader').show();
                $('.loading-cover').show();
            },
    success: function(data, textStatus, jqXHR) {
      //                   QUEUE INIT
      $.each(data, function(index) {
        $messages.push(data[index].messageTitle);
        var $i = index + 1; // avoid index 0
        var $id = data[index].messageTitle.split(' ').join('_'); //join value to apply as element id

        //add table rows as per data cels in DB
        $("#queue-table").find('tbody')
          .append($('<tr id=' + $id +
            '><th>' + $i + '</th><td>' +
            data[index].messageTitle +
            '</td>\n\
                                       <td><input type=' +
            'text ' + 'style=' + 'width:4em;' + '></td>' +
            '<td><input type=' + 'text ' + 'class=' + 'one' +
            ' style=' + 'width:4em;' +
            ' id="datetimepicker"></td><td><img src=' +
            'resources/images/ui-icons/icon-config-queue3.png' +
            ' width=' + '25' +
            ' class=' + 'icon-queue-msg-config ' + ' /></td>'));
      });
    },
    error: function(data, textStatus, jqXHR) {
      console.log("ajax call failed");
      console.log("textStatus = " + textStatus);
      console.log("jqXHR s= " + jqXHR);
      console.log("jqXHR.status = " + jqXHR.status);
    }
  });
  
    
      // Messages Queue Order Selection 

      // Loop into field inputs to asign Datepicker id to only one inputfield
      $('#queue-table').hover(function(e) {
        var $inputs = $('#queue-table').find('input');
        var one = [];
        one.push($inputs);

        // Print Queue
        for (var i = 0; i < one.length; i++) {
          console.log(one[i]);
        }
      });

      $(".icon-queue-msg-config").on('click', function(event) {
        $('#blur-screen').show();
        $('#video-edit-form').show();
      });
      $('#queue-table td').click(function(event) {
        $('#messages-table tr').not(this).removeClass(
          'selectRow');
        $(this).toggleClass('selectRow');
      });

      $('#queue-table td').hover(function() {
        $(this).css({
          backgroundColor: "#bfc7d3",
          cursor: 'pointer'
        });
      });
      $('#queue-table tr').mouseleave(function() {
        $(this).children('td').css("background-color", "white"); // or whatever
      });

      // DatePicker
      $('#datetimepicker').datetimepicker({
        datepicker: true
      });


// Video Editor Panel

  $('#video_upload').on("change", function(event) {
    var property = document.getElementById("form_position_1a")[1].files[0];
    var form_data = new FormData();
    form_data.append("file", property);
    
    //Posting media files for processing
    $.ajax({
      url: "/save_video",
      method: "POST",
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {
        alert(response);
      }
    });

  });

  var $video = $('#video').val();
  var $video_upload = $('#video_upload').val();

  var $title = $('#title').val();
  var $title_color = $('#title_color').val();

  var $main_img = $('#main_img').val();
  var $main_img_upload = $('#main_img_upload').val();

  var $left_img = $('#left_img').val();
  var $left_img_upload = $('#left_img_upload').val();

  var $right_img = $('#right_img').val();
  var $right_img_upload = $('#right_img_upload').val();

  var $audio_file = $('#audio_file').val();
  var $audio_file_upload = $('#audio_file_upload').val();

  var $text1 = $('#text1').val();
  var $text1_color = $('#text1_color').val();

  var $text2 = $('#text2').val();
  var $text2_color = $('#text2_color').val();

  var $text3 = $('#text3').val();
  var $text3_color = $('#text3_color').val();

// Data for video processing
  var new_video_config_data = {
        video:  $video,
        video_upload: $video_upload,
        title : $title,
        title_upload : $title_color,
        main_img : $main_img,
        main_img_upload : $main_img_upload,
        left_img : $left_img,
        left_img_upload : $left_img_upload,
        right_img : $right_img,
        right_img_upload : $right_img_upload,
        img : $audio_file,
        img_upload : $audio_file_upload,
        txt1 : $text1,
        text1_color : $text1_color,
        text2 : $text2,
        text2_color : $text2_color,
        text3 : $text3,
        text3_color : $text3_color
      };
      
}); //end of Doc.ready




